package google.com.ulam;

public class GridItemPembayaran {

    String gambar,namaikan,totalpembayaran,idpemesanan,totalpembelian;

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getNamaikan() {

        return namaikan;

    }

    public void setNamaikan(String namaikan) {

        this.namaikan = namaikan;

    }

    public String getTotalpembayaran() {

        return totalpembayaran;

    }

    public void setTotalpembayaran(String totalpembayaran) {

        this.totalpembayaran = totalpembayaran;

    }

    public String getIdpemesanan() {

        return idpemesanan;

    }

    public void setIdpemesanan(String idpemesanan) {

        this.idpemesanan = idpemesanan;

    }

    public String getTotalpembelian() {

        return totalpembelian;

    }

    public void setTotalpembelian(String totalpembelian) {

        this.totalpembelian = totalpembelian;

    }
}