package google.com.ulam;

public class Config {

    public static String LOGINUSER_URL = "/ulam/loginuser.php";
    public static String LOGINPEMBELI_URL = "/ulam/loginpembeli.php";
    public static String REGISTERUSER_URL = "/ulam/registeruser.php";
    public static String REGISTERPEMBELI_URL = "/ulam/registerpembeli.php";

    public static String BUATPESANAN_URL = "/ulam/buatpesanan.php";

    public static String HALAMANUTAMA_URL="/ulam/halamanutama.php";

    public static String PEMBAYARAN_URL="/ulam/pembayaran.php";

    public static String RIWAYATPEMESANAN_URL="/ulam/riwayatpemesanan.php";
    public static String RIWAYATPENJUALAN_URL="/ulam/riwayatpenjualan.php";

    public static String DETAILPEMESANAN_URL="/ulam/detailpemesanan.php";

    public static String UNGGAHBUKTI_URL="/ulam/unggahbukti.php";
    public static String UNGGAHIKAN_URL="/ulam/unggahikan.php";

    public static String KERANJANG_URL="/ulam/keranjangbelanja.php";
    public static String PENGIRIMAN_URL="/ulam/pengiriman.php";
    public static String KECAMATAN_URL="/ulam/kecamatan.php";
    public static String KABUPATEN_URL="/ulam/kabupaten.php";

    public static String ONGKIR_URL="/ulam/ongkir.php";

}