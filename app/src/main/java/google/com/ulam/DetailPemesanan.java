package google.com.ulam;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class DetailPemesanan extends AppCompatActivity {

    TextView detailpemesananidpembeli,detailpemesanannamapembeli,detailpemesananperusahaanpembeli,detailpemesananalamatpembeli,detailpemesanannomorpemesanan,detailpemesanannamaikan,detailpemesananhargaikan,detailpemesananjumlahikan,detailpemesanantanggalpemesanan,detailpemesananjenispengiriman,detailpemesananareapengiriman,detailpemesananbiayapengiriman,detailpemesanantotalpembelian,detailpemesanantotalpembayaran;

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDPEMBELI = "idpembeli";
    private static final String TAG_NAMAPEMBELI = "namapembeli";
    private static final String TAG_PERUSAHAANPEMBELI= "perusahaanpembeli";
    private static final String TAG_ALAMATPEMBELI= "alamatpembeli";
    private static final String TAG_NOMORPEMESANAN= "nomorpemesanan";
    private static final String TAG_NAMAIKAN= "namaikan";
    private static final String TAG_HARGAIKAN= "hargaikan";
    private static final String TAG_JUMLAHIKAN= "jumlahpemesanan";
    private static final String TAG_TANGGALPEMESANAN= "tanggalpemesanan";
    private static final String TAG_JENISPENGIRIMAN= "pengirimanpemesanan";
    private static final String TAG_AREAPENGIRIMAN= "namaareapengiriman";
    private static final String TAG_BIAYAPENGIRIMAN= "biayapengiriman";
    private static final String TAG_TOTALPEMBELIAN= "totalpemesanan";
    private static final String TAG_TOTALPEMBAYARAN= "totalpembayaran";

    static boolean a=false;

    JSONArray rs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pemesanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        detailpemesananidpembeli = (TextView) findViewById(R.id.detailpemesananidpembeli);
        detailpemesanannamapembeli = (TextView) findViewById(R.id.detailpemesanannamapembeli);
        detailpemesananperusahaanpembeli = (TextView) findViewById(R.id.detailpemesananperusahaanpembeli);
        detailpemesananalamatpembeli = (TextView) findViewById(R.id.detailpemesananalamatpembeli);
        detailpemesanannomorpemesanan = (TextView) findViewById(R.id.detailpemesanannomorpemesanan);
        detailpemesanannamaikan = (TextView) findViewById(R.id.detailpemesanannamaikan);
        detailpemesananhargaikan = (TextView) findViewById(R.id.detailpemesananhargaikan);
        detailpemesananjumlahikan = (TextView) findViewById(R.id.detailpemesananjumlahikan);
        detailpemesanantanggalpemesanan = (TextView) findViewById(R.id.detailpemesanantanggalpemesanan);
        detailpemesananjenispengiriman = (TextView) findViewById(R.id.detailpemesananjenispengiriman);
        detailpemesananareapengiriman = (TextView) findViewById(R.id.detailpemesananareapengiriman);
        detailpemesananbiayapengiriman = (TextView) findViewById(R.id.detailpemesananbiayapengiriman);
        detailpemesanantotalpembelian = (TextView) findViewById(R.id.detailpemesanantotalpembelian);
        detailpemesanantotalpembayaran = (TextView) findViewById(R.id.detailpemesanantotalpembayaran);

        new SendDetail().execute();
    }

    //JSON parser, mengambil data dari web service,
    private class SendDetail extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            String URL;
            URL = "http://";
            URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            URL += Config.DETAILPEMESANAN_URL;
            URL += "?idpemesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idpemesanan","");

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);

                        String idpembeli = a.getString(TAG_IDPEMBELI);
                        String namapembeli = a.getString(TAG_NAMAPEMBELI);
                        String perusahaanpembeli = a.getString(TAG_PERUSAHAANPEMBELI);
                        String alamatpembeli = a.getString(TAG_ALAMATPEMBELI);
                        String nomorpemesanan = a.getString(TAG_NOMORPEMESANAN);
                        String namaikan = a.getString(TAG_NAMAIKAN);
                        String hargaikan = a.getString(TAG_HARGAIKAN);
                        String jumlahikan = a.getString(TAG_JUMLAHIKAN);
                        String tanggalpemesanan = a.getString(TAG_TANGGALPEMESANAN);
                        String jenispengiriman = a.getString(TAG_JENISPENGIRIMAN);
                        String areapengiriman = a.getString(TAG_AREAPENGIRIMAN);
                        String biayapengiriman = a.getString(TAG_BIAYAPENGIRIMAN);
                        String totalpembelian = a.getString(TAG_TOTALPEMBELIAN);
                        String totalpembayaran = a.getString(TAG_TOTALPEMBAYARAN);

                        detailpemesananidpembeli.setText(idpembeli);
                        detailpemesanannamapembeli.setText(namapembeli);
                        detailpemesananperusahaanpembeli.setText(perusahaanpembeli);
                        detailpemesananalamatpembeli.setText(alamatpembeli);
                        detailpemesanannomorpemesanan.setText(nomorpemesanan);
                        detailpemesanannamaikan.setText(namaikan);
                        detailpemesananhargaikan.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(hargaikan)).replace(",",".")));
                        detailpemesananjumlahikan.setText(jumlahikan+" Ton");
                        detailpemesanantanggalpemesanan.setText(tanggalpemesanan);
                        detailpemesananjenispengiriman.setText(jenispengiriman);
                        detailpemesananareapengiriman.setText(areapengiriman);
                        detailpemesananbiayapengiriman.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(biayapengiriman)).replace(",",".")));
                        detailpemesanantotalpembelian.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(totalpembelian)*1000).replace(",",".")));
                        detailpemesanantotalpembayaran.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(totalpembayaran)).replace(",",".")));

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
