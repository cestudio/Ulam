package google.com.ulam;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class PagerAdapterRegister extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapterRegister(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RegisterUserFragment tab0 = new RegisterUserFragment();
                return tab0;
            case 1:
                RegisterPembeliFragment tab1 = new RegisterPembeliFragment();
                return tab1;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}