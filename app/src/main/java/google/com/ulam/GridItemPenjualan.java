package google.com.ulam;


public class GridItemPenjualan {

    String gambar,namaikan,totalpenjualan,jumlahpenjualan,idpemesanan,tanggalpenjualan;

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getNamaikan() {

        return namaikan;

    }

    public void setNamaikan(String namaikan) {

        this.namaikan = namaikan;

    }

    public String getTotalpenjualan() {

        return totalpenjualan;

    }

    public void setTotalpenjualan(String totalpenjualan) {

        this.totalpenjualan = totalpenjualan;

    }

    public String getIdpemesanan() {

        return idpemesanan;

    }

    public void setIdpemesanan(String idpemesanan) {

        this.idpemesanan = idpemesanan;

    }

    public String getJumlahpenjualan() {

        return jumlahpenjualan;

    }

    public void setJumlahpenjualan(String jumlahpenjualan) {

        this.jumlahpenjualan = jumlahpenjualan;

    }


    public String getTanggalpenjualan() {

        return tanggalpenjualan;

    }

    public void setTanggalpenjualan(String tanggalpenjualan) {

        this.tanggalpenjualan = tanggalpenjualan;

    }
}