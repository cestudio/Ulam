package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RiwayatPenjualan extends AppCompatActivity {

    public static Activity fa;

    //Web api url
    public static String FEED_URL;

    private static final String TAG = RiwayatPenjualan.class.getSimpleName();

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterPenjualan mGridAdapter;
    private ArrayList<GridItemPenjualan> mGridData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_penjualan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //grid view
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterPenjualan(RiwayatPenjualan.this, R.layout.grid_item_penjualan, mGridData);
        mGridView.setAdapter(mGridAdapter);

        FEED_URL = "http://";
        FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.RIWAYATPENJUALAN_URL;
        FEED_URL += "?iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

        //Mulai download
        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

    }

    //Mengunduh data
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // sukses
                } else {
                    result = 0; //gagal
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete
            if (result == 1) {
                mProgressBar.setVisibility(View.GONE);
                mGridAdapter.setGridData(mGridData);
            } else {
                NodataPenjualan nodata = new NodataPenjualan(RiwayatPenjualan.this);
                nodata.setCancelable(false);
                nodata.show();
            }
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemPenjualan item;

            if(posts.length()<=0) {
                NodataPenjualan nodata = new NodataPenjualan(RiwayatPenjualan.this);
                nodata.show();
            }

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idpemesanan = post.optString("idpemesanan");
                String namaikan = post.optString("namaikan");
                String totalpenjualan = post.optString("totalpenjualan");
                String jumlahpenjualan = post.optString("jumlahpenjualan");
                String tanggalpenjualan = post.optString("tanggalpenjualan");
                item = new GridItemPenjualan();
                item.setIdpemesanan(idpemesanan);
                item.setNamaikan(namaikan);
                item.setTotalpenjualan(totalpenjualan);
                item.setJumlahpenjualan(jumlahpenjualan);
                item.setTanggalpenjualan(tanggalpenjualan);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString("gambarikan"));
                }

                mGridData.add(item);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
