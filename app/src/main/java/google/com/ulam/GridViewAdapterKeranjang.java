package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class GridViewAdapterKeranjang extends ArrayAdapter<GridItem> {
    ArrayList<String> mylist = new ArrayList<String>();

    String idkeranjang;

    GridItem item;

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItem> mGridData = new ArrayList<GridItem>();

    public GridViewAdapterKeranjang(Context mContext, int layoutResourceId, ArrayList<GridItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        idkeranjang = "keranjang"+getContext().getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("iduser","");

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();

            if (Keranjang.mylistidikan.size()>0) {
                Keranjang.checkout.setEnabled(true);
            }
            else{
                Keranjang.checkout.setEnabled(false);
            }

            holder.idikan = (TextView) row.findViewById(R.id.idikan);
            holder.namaikan = (TextView) row.findViewById(R.id.namaikan);
            holder.hargaikan = (TextView) row.findViewById(R.id.hargaikan);
            holder.datahargaikan = (TextView) row.findViewById(R.id.nilaihargaikan);
            holder.stokikan = (TextView) row.findViewById(R.id.stokikan);
            holder.datastokikan = (TextView) row.findViewById(R.id.nilaistokikan);
            holder.gambarikan = (ImageView) row.findViewById(R.id.gambarikan);
            holder.cbkeranjang = (CheckBox) row.findViewById(R.id.cbkeranjang);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        item = mGridData.get(position);
        holder.idikan.setText(Html.fromHtml(item.getId()));
        holder.namaikan.setText(Html.fromHtml(item.getNamaikan()));
        holder.hargaikan.setText("Harga = Rp. "+Html.fromHtml(item.getHarga()));
        holder.datahargaikan.setText(Html.fromHtml(item.getHarga()));
        holder.stokikan.setText("Stok = "+Html.fromHtml(item.getStok())+" Ton");
        holder.datastokikan.setText(Html.fromHtml(item.getStok()));
        holder.cbkeranjang.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //is chkIos checked?
                if (((CheckBox) v).isChecked()) {

                    Keranjang.mylistidikan.add(holder.idikan.getText().toString() );
                    Keranjang.mylistidnamaikan.add(holder.namaikan.getText().toString());
                    Keranjang.mylistidhargaikan.add(holder.datahargaikan.getText().toString());
                    Keranjang.mylistidstokikan.add(holder.datastokikan.getText().toString());

                    Log.e("keranjang valid = ",Keranjang.mylistidikan.toString());

                    if (Keranjang.mylistidikan.size()>0) {
                        Keranjang.checkout.setEnabled(true);
                    }
                    else{
                        Keranjang.checkout.setEnabled(false);
                    }
                }
                else {
                    Keranjang.mylistidikan.remove(holder.idikan.getText().toString());
                    Keranjang.mylistidnamaikan.remove(holder.namaikan.getText().toString());
                    Keranjang.mylistidhargaikan.remove(holder.datahargaikan.getText().toString());
                    Keranjang.mylistidstokikan.remove(holder.datastokikan.getText().toString());

                    Log.e("keranjang valid = ",Keranjang.mylistidikan.toString());

                    if (Keranjang.mylistidikan.size()>0) {
                        Keranjang.checkout.setEnabled(true);
                    }
                    else{
                        Keranjang.checkout.setEnabled(false);
                    }
                }

            }
        });
        Picasso.with(mContext).load(item.getGambar()).resize(500,500).into(holder.gambarikan);

        return row;
    }

    static class ViewHolder {
        TextView idikan,namaikan,hargaikan,stokikan,datahargaikan,datastokikan;
        ImageView gambarikan;
        CheckBox cbkeranjang;
    }
}