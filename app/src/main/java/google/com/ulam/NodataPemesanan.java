package google.com.ulam;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;


public class NodataPemesanan extends Dialog {
    public Activity c;

    public NodataPemesanan(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutnodata);

        Button btnok = (Button) findViewById(R.id.btnok);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (c.getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("jenisnodata","").equals("pembayaran")) {
                    Pembayaran.fa.finish();
                }
                else{
                    RiwayatPemesanan.fa.finish();
                }
            }
        });
    }
}
