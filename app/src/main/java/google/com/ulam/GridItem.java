package google.com.ulam;

public class GridItem {

    String gambar,namaikan,harga,id,stok;

    public String getGambar() {

        return gambar;

    }

    public void setGambar(String gambar) {

        this.gambar = gambar;

    }

    public String getNamaikan() {

        return namaikan;

    }

    public void setNamaikan(String namaikan) {

        this.namaikan = namaikan;

    }

    public String getHarga() {

        return harga;

    }

    public void setHarga(String harga) {

        this.harga = harga;

    }

    public String getId() {

        return id;

    }

    public void setId(String id) {

        this.id = id;

    }

    public String getStok() {

        return stok;

    }

    public void setStok(String stok) {

        this.stok = stok;

    }
}