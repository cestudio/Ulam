package google.com.ulam;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RegisterPembeliFragment extends Fragment {
    Button simpan,batal;

    EditText nama,namaperusahaan,alamat,username,password;

    private static final String TAG_RESULT = "result";
    private static final String TAG_PENDAFTARAN = "pendaftaran";

    static boolean a=false;

    JSONArray rs = null;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_register_pembeli, container, false);

        nama = (EditText) rootview.findViewById(R.id.daftarnama);
        namaperusahaan = (EditText) rootview.findViewById(R.id.daftarnamaperusahaan);
        alamat = (EditText) rootview.findViewById(R.id.daftaralamat);
        username = (EditText) rootview.findViewById(R.id.daftarusername);
        password = (EditText) rootview.findViewById(R.id.daftarpassword);

        simpan = (Button) rootview.findViewById(R.id.daftarsimpan);
        batal = (Button) rootview.findViewById(R.id.daftarkembali);

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendRegister().execute();
            }
        });

        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),Login.class));
                Register.fa.finish();
            }
        });

        return rootview;
    }

    private class SendRegister extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses
        @Override
        protected JSONObject doInBackground(String... args)
        {
            String URL;
            URL = "http://";
            URL += getActivity().getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
            URL += Config.REGISTERPEMBELI_URL;

            URL += "?nama="+nama.getText().toString().replace(" ","%20");
            URL += "&namaperusahaan="+namaperusahaan.getText().toString().replace(" ","%20");
            URL += "&alamat="+alamat.getText().toString().replace(" ","%20");
            URL += "&username="+username.getText().toString().replace(" ","%20");
            URL += "&password="+password.getText().toString().replace(" ","%20");

            Log.e("Daftar = ",URL);

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    //mengambil array
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String pendaftaran = a.getString(TAG_PENDAFTARAN);
                        if (pendaftaran.equals("0")){
                            Toast.makeText(getActivity(), "Pendaftaran tidak berhasil, coba lagi!", Toast.LENGTH_SHORT).show();
                        }
                        else if (pendaftaran.equals("1")){
                            Toast.makeText(getActivity(), "Username anda sudah terdaftar!", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage("Pendaftaran berhasil!");
                            alertDialogBuilder.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(getActivity(),Login.class));
                                    Register.fa.finish();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down
            else {

                Toast.makeText(getActivity(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}