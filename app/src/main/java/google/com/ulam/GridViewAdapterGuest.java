package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class GridViewAdapterGuest extends ArrayAdapter<GridItem> {

    GridItem item;

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItem> mGridData = new ArrayList<GridItem>();

    public GridViewAdapterGuest(Context mContext, int layoutResourceId, ArrayList<GridItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idikan = (TextView) row.findViewById(R.id.idikan);
            holder.namaikan = (TextView) row.findViewById(R.id.namaikan);
            holder.hargaikan = (TextView) row.findViewById(R.id.hargaikan);
            holder.inthargaikan = (TextView) row.findViewById(R.id.inthargaikan);
            holder.stokikan = (TextView) row.findViewById(R.id.stokikan);
            holder.intstokikan = (TextView) row.findViewById(R.id.intstokikan);
            holder.gambarikan = (ImageView) row.findViewById(R.id.gambarikan);
            holder.tambahkeranjang = (Button) row.findViewById(R.id.btntambahkeranjang);
            holder.beli = (Button) row.findViewById(R.id.btnbeli);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        item = mGridData.get(position);
        holder.idikan.setText(Html.fromHtml(item.getId()));
        holder.namaikan.setText(Html.fromHtml(item.getNamaikan()));
        holder.hargaikan.setText("Harga = Rp. "+Html.fromHtml(item.getHarga()));
        holder.stokikan.setText("Stok = "+Html.fromHtml(item.getStok())+" Ton");
        holder.inthargaikan.setText(Html.fromHtml(item.getHarga()));
        holder.intstokikan.setText(Html.fromHtml(item.getStok()));
        holder.tambahkeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setMessage("Anda harus login terlebih dahulu");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setNegativeButton("Login",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getContext().startActivity(new Intent(getContext(),Login.class));
                    }
                });
                alertDialogBuilder.setPositiveButton("Nanti",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        holder.beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setMessage("Anda harus login terlebih dahulu");
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setNegativeButton("Login",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getContext().startActivity(new Intent(getContext(),Login.class));
                    }
                });
                alertDialogBuilder.setPositiveButton("Nanti",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Picasso.with(mContext).load(item.getGambar()).resize(500,500).into(holder.gambarikan);

        return row;
    }

    static class ViewHolder {
        TextView idikan,namaikan,hargaikan,stokikan,inthargaikan,intstokikan;
        ImageView gambarikan;
        Button tambahkeranjang,beli;
    }
}