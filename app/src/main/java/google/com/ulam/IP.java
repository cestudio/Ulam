package google.com.ulam;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;


public class IP extends Dialog {
    public Activity c;
    public Button simpan;
    public EditText etIP;

    public IP(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layoutip);
        etIP = (EditText) findViewById(R.id.editTextIP);
        simpan = (Button) findViewById(R.id.btnsimpan);

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor1 = c.getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                editor1.putString("IP", etIP.getText().toString());
                editor1.commit();
                dismiss();
                Intent i = c.getBaseContext().getPackageManager().getLaunchIntentForPackage(c.getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                c.startActivity(i);
            }
        });
    }
}
