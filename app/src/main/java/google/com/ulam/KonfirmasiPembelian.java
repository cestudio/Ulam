package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class KonfirmasiPembelian extends AppCompatActivity {

    TextView idpembeli,namapembeli,alamatpembeli,namaikan,hargaikan,jumlahikan,jenispengiriman,areapengiriman,totalpembelian,biayapengiriman,totalpembayaran;

    private static final String TAG_RESULT = "result";
    private static final String TAG_PEMESANAN = "pemesanan";

    static boolean a=false;

    JSONArray rs = null;

    Button btnpembelian;

    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_pembelian);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;

        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();

        idpembeli = (TextView) findViewById(R.id.kpid);
        namapembeli = (TextView) findViewById(R.id.kpnama);
        alamatpembeli = (TextView) findViewById(R.id.kpalamat);
        namaikan = (TextView) findViewById(R.id.kpnamaikan);
        hargaikan = (TextView) findViewById(R.id.kphargaikan);
        jumlahikan = (TextView) findViewById(R.id.kpjumlahikan);
        jenispengiriman = (TextView) findViewById(R.id.kpjenispengiriman);
        areapengiriman = (TextView) findViewById(R.id.kpareapengiriman);
        biayapengiriman = (TextView) findViewById(R.id.kpbiayapengiriman);
        totalpembelian = (TextView) findViewById(R.id.kptotalpembelian);
        totalpembayaran = (TextView) findViewById(R.id.kptotalpembayaran);
        btnpembelian = (Button) findViewById(R.id.kpbtnpembelian);

        idpembeli.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser",""));
        namapembeli.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("nama",""));
        alamatpembeli.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("alamat",""));
        namaikan.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("namaikan",""));
        hargaikan.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("hargaikan","")))).replace(",","."));
        jumlahikan.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("jumlahikan","")+" Ton");
        jenispengiriman.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("jenispengiriman",""));
        areapengiriman.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("areapengiriman",""));
        editor.putString("totalpembelian", String.valueOf((int)(Float.parseFloat(getSharedPreferences("DATA",MODE_PRIVATE).getString("hargaikan",""))*(Float.parseFloat(getSharedPreferences("DATA",MODE_PRIVATE).getString("jumlahikan","")))*1000))).commit();
        totalpembelian.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(getSharedPreferences("DATA",Context.MODE_PRIVATE).getString("totalpembelian","")))).replace(",","."));
        biayapengiriman.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(getSharedPreferences("DATA",MODE_PRIVATE).getString("biayapengiriman","")))).replace(",","."));

        editor.commit();

        editor.putString("totalpembayaran", String.valueOf((int)(Float.parseFloat(getSharedPreferences("DATA",MODE_PRIVATE).getString("biayapengiriman",""))+Float.parseFloat(getSharedPreferences("DATA",MODE_PRIVATE).getString("totalpembelian","")))));

        editor.commit();

        totalpembayaran.setText("Rp. "+String.valueOf(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(getSharedPreferences("DATA",MODE_PRIVATE).getString("totalpembayaran","")))).replace(",","."));

        btnpembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendPesanan().execute();
            }
        });
    }

    private class SendPesanan extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        //menjalankan proses
        @Override
        protected JSONObject doInBackground(String... args)
        {
            String URL;
            URL = "http://";
            URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
            URL += Config.BUATPESANAN_URL;

            URL += "?jumlahpesan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("jumlahikan","").replace(" ","%20");
            URL += "&totalpemesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("totalpembayaran","").replace(" ","%20");
            URL += "&pengiriman="+getSharedPreferences("DATA",MODE_PRIVATE).getString("jenispengiriman","").replace(" ","%20");
            URL += "&namaarea="+getSharedPreferences("DATA",MODE_PRIVATE).getString("areapengiriman","").replace(" ","%20");
            URL += "&idikan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idikan","").replace(" ","%20");
            URL += "&idpembeli="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","").replace(" ","%20");

            Log.e("Buat pesanan =",URL);

            JSONParser jParser = new JSONParser();

            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                a=false;
            }
            else a=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(a==true)
            {
                try{
                    Log.e("status",a+"");
                    //mengambil array
                    rs = json.getJSONArray(TAG_RESULT);
                    //loop
                    for(int i=0; i<rs.length();i++)
                    {
                        JSONObject a = rs.getJSONObject(i);
                        String pemesanan = a.getString(TAG_PEMESANAN);
                        if (pemesanan.equals("0")){
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(KonfirmasiPembelian.this);
                            alertDialogBuilder.setMessage("Gagal membuat pesanan!");
                            alertDialogBuilder.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                        else{
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(KonfirmasiPembelian.this);
                            alertDialogBuilder.setMessage("Pemesanan Berhasil!");
                            alertDialogBuilder.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    if (Keranjang.statkeranjang<Keranjang.mylistidikan.size()-1) {
                                        FormPembelian.fa.finish();//fa = konfirmasi pembelian
                                        Keranjang.statkeranjang++;//lebih dari 1 kerangjang akan kembali ke form pembelian
                                        startActivity(new Intent(KonfirmasiPembelian.this,FormPembelian.class));
                                    }
                                    else{
                                        Halaman_Utama_Pembeli.mylist.clear();
                                        Halaman_Utama_Pembeli.keranjang.setText("0");
                                        FormPembelian.fa.finish();
                                        if(getSharedPreferences("DATA",MODE_PRIVATE).getString("statuspembelian","").equals("beli")){

                                        }
                                        else{
                                            Keranjang.fa.finish();
                                        }

                                    }
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down
            else {

                Toast.makeText(KonfirmasiPembelian.this, "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status",a+"");}
        }

    }
}
