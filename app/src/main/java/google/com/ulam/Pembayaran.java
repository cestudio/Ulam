package google.com.ulam;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Pembayaran extends AppCompatActivity {

    //Web api url
    public static String FEED_URL;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterPembayaran mGridAdapter;
    private ArrayList<GridItemPembayaran> mGridData;


    // LogCat tag
    private static final String TAG = Pembayaran.class.getSimpleName();

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static final int MEDIA_TYPE_IMAGE = 1;

    private String filePath = null;
    private Uri fileUri;

    long totalSize = 0;

    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
        editor.putString("jenisnodata", "pembayaran");
        editor.commit();

        fa = this;

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //grid view
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterPembayaran(Pembayaran.this, R.layout.grid_item_pembayaran, mGridData);
        mGridView.setAdapter(mGridAdapter);

        FEED_URL = "http://";
        FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.PEMBAYARAN_URL;
        FEED_URL += "?idpembeli="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

        //Start download
        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                isStoragePermissionGranted();
                //Get item at position
                GridItemPembayaran item = (GridItemPembayaran) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("idpemesanan", item.getIdpemesanan());
                editor.putString("jenisnodata", "pembayaran");
                editor.commit();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Pembayaran.this);
                alertDialogBuilder.setMessage("Apa anda ingin mengonfirmasi pembayaran?");
                alertDialogBuilder.setNegativeButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        captureImage();
                    }
                });
                alertDialogBuilder.setPositiveButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permintaan disetujui");
                return true;
            } else {

                Log.v(TAG,"Permintaan tidak disetujui");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else {
            Log.v(TAG,"Permintaan disetujui");
            return true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("file_uri", fileUri);
    }

    /**
     * mengunggah ke server
     * */
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.UNGGAHBUKTI_URL;
            FEED_URL += "?idpemesanan="+getSharedPreferences("DATA",MODE_PRIVATE).getString("idpemesanan","");

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(FEED_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(fileUri.getPath());

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Terjadi kesalahan! Kode Status Http: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Respon dari server: " + result);
            showAlert(result);
            super.onPostExecute(result);
        }

    }

    /**
     * pemberitahuan
     * */
    private void showAlert(String message) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Proses upload selesai!")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        filePath = fileUri.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);


    }

    /**
     *
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                if (filePath != null) {
                    // Menampilkan gambar
                    new UploadFileToServer().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "File tidak ditemukan", Toast.LENGTH_LONG).show();
                }

            } else if (resultCode == RESULT_CANCELED) {

                // membatalkan pengambilan gambar
                Toast.makeText(getApplicationContext(),
                        "Batal mengambil gambar", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // gagal mengambil gambar
                Toast.makeText(getApplicationContext(),
                        "Gagal mengambil gambar", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image
     */
    private static File getOutputMediaFile(int type) {

        // Lokasi sdcard eksternal
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Download");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Gagal membuat folder "
                        + "Download" + " directory");
                return null;
            }
        }

        // membuat nama gambar
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    //Downloading data
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete
            if (result == 1) {
                mProgressBar.setVisibility(View.GONE);
                mGridAdapter.setGridData(mGridData);
            } else {
                NodataPembayaran nodata = new NodataPembayaran(Pembayaran.this);
                nodata.setCancelable(false);
                nodata.show();
            }
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemPembayaran item;

            if(posts.length()<=0) {
                NodataPembayaran nodata = new NodataPembayaran(Pembayaran.this);
                nodata.show();
            }

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idpemesanan = post.optString("idpemesanan");
                String namaikan = post.optString("namaikan");
                String totalpembayaran = post.optString("totalpembayaran");
                String totalpembelian = post.optString("totalpemesanan");
                item = new GridItemPembayaran();
                item.setIdpemesanan(idpemesanan);
                item.setNamaikan(namaikan);
                item.setTotalpembayaran(totalpembayaran);
                item.setTotalpembelian(totalpembelian);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString("gambarikan"));
                }

                mGridData.add(item);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
