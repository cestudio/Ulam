package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterPembayaran extends ArrayAdapter<GridItemPembayaran> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemPembayaran> mGridData = new ArrayList<GridItemPembayaran>();

    public GridViewAdapterPembayaran(Context mContext, int layoutResourceId, ArrayList<GridItemPembayaran> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items
     */
    public void setGridData(ArrayList<GridItemPembayaran> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idpemesanan = (TextView) row.findViewById(R.id.idpemesanan);
            holder.namaikan = (TextView) row.findViewById(R.id.namaikan);
            holder.totalpembayaran = (TextView) row.findViewById(R.id.totalpembayaran);
            holder.totalpembelian = (TextView) row.findViewById(R.id.totalpembelian);
            holder.gambarikan = (ImageView) row.findViewById(R.id.gambarikan);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemPembayaran item = mGridData.get(position);
        holder.idpemesanan.setText("Nomor Pemesanan. "+Html.fromHtml(item.getIdpemesanan()));
        holder.namaikan.setText("Nama Ikan : "+Html.fromHtml(item.getNamaikan()));
        holder.totalpembayaran.setText("Total Pembayaran : Rp. "+Html.fromHtml(item.getTotalpembayaran()));
        holder.totalpembelian.setText("Jumlah Pemesanan : "+Html.fromHtml(item.getTotalpembelian())+" Ton");

        Picasso.with(mContext).load(item.getGambar()).resize(500,500).into(holder.gambarikan);

        return row;
    }

    static class ViewHolder {
        TextView idpemesanan,namaikan,totalpembayaran,totalpembelian;
        ImageView gambarikan;
    }
}