package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapterPenjualan extends ArrayAdapter<GridItemPenjualan> {

    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItemPenjualan> mGridData = new ArrayList<GridItemPenjualan>();

    public GridViewAdapterPenjualan(Context mContext, int layoutResourceId, ArrayList<GridItemPenjualan> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }


    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItemPenjualan> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.idpenjualan = (TextView) row.findViewById(R.id.idpenjualan);
            holder.namaikan = (TextView) row.findViewById(R.id.namaikan);
            holder.totalpenjualan = (TextView) row.findViewById(R.id.totalpenjualan);
            holder.jumlahpenjualan = (TextView) row.findViewById(R.id.jumlahpenjualan);
            holder.gambarikan = (ImageView) row.findViewById(R.id.gambarikan);
            holder.tanggalpenjualan = (TextView) row.findViewById(R.id.tanggalpenjualan);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItemPenjualan item = mGridData.get(position);
        holder.idpenjualan.setText("Nomor Pemesanan. "+Html.fromHtml(item.getIdpemesanan()));
        holder.namaikan.setText(Html.fromHtml(item.getNamaikan()));
        holder.totalpenjualan.setText("Jumlah Pendapatan : Rp. "+Html.fromHtml(item.getTotalpenjualan()));
        holder.jumlahpenjualan.setText("Jumlah penjualan : "+Html.fromHtml(item.getJumlahpenjualan())+" Ton");
        holder.tanggalpenjualan.setText("Tanggal Terjual : "+Html.fromHtml(item.getTanggalpenjualan()));

        Picasso.with(mContext).load(item.getGambar()).resize(500,500).into(holder.gambarikan);

        return row;
    }

    static class ViewHolder {
        TextView idpenjualan,namaikan,totalpenjualan,jumlahpenjualan,tanggalpenjualan;
        ImageView gambarikan;
    }
}