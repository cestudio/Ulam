package google.com.ulam;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FirebaseIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
        editor.putString("token", refreshedToken);
        editor.commit();

    }

    private void sendRegistrationToServer(String token) {


    }
}