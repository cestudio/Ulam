package google.com.ulam;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;


public class SplashScreen extends AppCompatActivity {

    //Set waktu lama splashscreen
    private static int splashInterval = 4000;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    ImageView logo;
    Animation fadeOutAnimation,fadeInAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {// fungsi ketika dii
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splah_screen);
        logo = (ImageView) findViewById(R.id.logo);
        fadeInAnimation = new AlphaAnimation(0.0f, 2.0f);//animasi perpindahan
        fadeInAnimation.setDuration(4000);
        logo.startAnimation(fadeInAnimation);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                if (getSharedPreferences("DATA", MODE_PRIVATE).getString("statlogin", "").equals("1")) {
                    if (getSharedPreferences("DATA", MODE_PRIVATE).getString("status", "").equals("pengelola")) {
                        Intent intent = new Intent(SplashScreen.this, Halaman_Utama_User.class);
                        startActivity(intent);
                        finish();
                    } else if (getSharedPreferences("DATA", MODE_PRIVATE).getString("status", "").equals("pembeli")) {
                        Intent intent = new Intent(SplashScreen.this, Halaman_Utama_Pembeli.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Intent i = new Intent(SplashScreen.this, Halaman_Utama.class);
                    startActivity(i);
                    finish();
                }

            }
        }, splashInterval);

    }
}

