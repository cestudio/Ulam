package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RiwayatPemesanan extends AppCompatActivity {

    public static Activity fa;

    //Web api url
    public static String FEED_URL;

    private static final String TAG = RiwayatPemesanan.class.getSimpleName();

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterPembayaran mGridAdapter;
    private ArrayList<GridItemPembayaran> mGridData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_pemesanan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
        editor.putString("jenisnodata", "pemesanan");
        editor.commit();

        fa = this;

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //grid view
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterPembayaran(RiwayatPemesanan.this, R.layout.grid_item_pembayaran, mGridData);
        mGridView.setAdapter(mGridAdapter);

        FEED_URL = "http://";
        FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.RIWAYATPEMESANAN_URL;
        FEED_URL += "?idpembeli="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");

        //Start download
        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                GridItemPembayaran item = (GridItemPembayaran) parent.getItemAtPosition(position);

                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("idpemesanan", item.getIdpemesanan());
                editor.commit();

                startActivity(new Intent(RiwayatPemesanan.this,DetailPemesanan.class));
            }
        });
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mProgressBar.setVisibility(View.GONE);
                mGridAdapter.setGridData(mGridData);
            } else {
                NodataPemesanan nodata = new NodataPemesanan(RiwayatPemesanan.this);
                nodata.setCancelable(false);
                nodata.show();
            }
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItemPembayaran item;

            if(posts.length()<=0) {
                NodataPemesanan nodata = new NodataPemesanan(RiwayatPemesanan.this);
                nodata.show();
            }

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idpemesanan = post.optString("idpemesanan");
                String namaikan = post.optString("namaikan");
                String totalpembayaran = post.optString("totalpembayaran");
                String totalpembelian = post.optString("totalpemesanan");
                item = new GridItemPembayaran();
                item.setIdpemesanan(idpemesanan);
                item.setNamaikan(namaikan);
                item.setTotalpembayaran(totalpembayaran);
                item.setTotalpembelian(totalpembelian);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString("gambarikan"));
                }

                mGridData.add(item);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
