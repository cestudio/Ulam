package google.com.ulam;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UnggahIkan extends AppCompatActivity {

    public static String FEED_URL;

    // LogCat tag
    private static final String TAG = UnggahIkan.class.getSimpleName();

    private Button btCamera;
    private Button btnkirim;
    private Button btnBatal;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    public static final int MEDIA_TYPE_IMAGE = 1;

    private ImageView imageView;

    private String filePath = null;
    private Uri fileUri;

    TextView txtPercentage;
    EditText namaikan,hargaikan,ketersediaan;
    Spinner spinjenisikan;

    long totalSize = 0;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unggah_ikan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        namaikan = (EditText) findViewById(R.id.unggahikannamaikan);
        hargaikan = (EditText) findViewById(R.id.unggahikanhargaikan);
        ketersediaan = (EditText) findViewById(R.id.unggahikanketersediaanikan);
        spinjenisikan = (Spinner) findViewById(R.id.spinunggahikanjenisikan);

        btCamera = (Button) findViewById(R.id.btnunggahfotoikan);
        btnkirim = (Button) findViewById(R.id.btnunggahikankirim);
        btnBatal = (Button) findViewById(R.id.btnunggahikanbatal);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);

        imageView = (ImageView) findViewById(R.id.unggahfotoikan);//penempatan gambar yg telah diambil

        imageView.setVisibility(View.GONE);

        btCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStoragePermissionGranted();
                captureImage();
            }
        });

        btnkirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filePath!=null){
                    if(!namaikan.getText().toString().equals("")){
                        if(!hargaikan.getText().toString().equals("")){
                            if(!ketersediaan.getText().toString().equals("")){
                                if(spinjenisikan.getSelectedItemPosition()>0){
                                    new UploadFileToServer().execute();
                                }
                                else{
                                    Toast.makeText(UnggahIkan.this, "Anda harus mengisi jenis ikan!", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else{
                                Toast.makeText(UnggahIkan.this, "Anda harus mengisi ketersediaan ikan!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Toast.makeText(UnggahIkan.this, "Anda harus mengisi harga ikan!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(UnggahIkan.this, "Anda harus mengisi nama ikan!", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(UnggahIkan.this, "Anda harus memberikan foto ikan!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
//penyimpanan gambar
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permintaan diizinkan");
                return true;
            } else {

                Log.v(TAG,"Permintaan tidak diizinkan");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else {
            Log.v(TAG,"Permintaan diizinkan");
            return true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Simpan file url dalam bentuk bundel
        outState.putParcelable("file_uri", fileUri);
    }

    /**
     *
     Mengunggah file ke server
     * */
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar
            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Membuat progress bar terlihat
            progressBar.setVisibility(View.VISIBLE);

            // Memperbarui progress bar value
            progressBar.setProgress(progress[0]);

            // Memperbarui nilai persentase
            txtPercentage.setText(String.valueOf(progress[0]) + "%");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            FEED_URL = "http://";
            FEED_URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            FEED_URL += Config.UNGGAHIKAN_URL;
            FEED_URL += "?iduser="+getSharedPreferences("DATA",MODE_PRIVATE).getString("iduser","");
            FEED_URL += "&namaikan="+namaikan.getText().toString().replace(" ","%20");
            FEED_URL += "&jenisikan="+spinjenisikan.getSelectedItem().toString().replace(" ","%20");
            FEED_URL += "&hargaikan="+hargaikan.getText().toString().replace(" ","%20");
            FEED_URL += "&ketersediaanikan="+ketersediaan.getText().toString().replace(" ","%20");

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(FEED_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(fileUri.getPath());

                //Menambahkan data file
                entity.addPart("image", new FileBody(sourceFile));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Membuat panggilan server
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Terjadi kesalahan! Kode Status Http: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Respon dari server: " + result);
            showAlert(result);
            super.onPostExecute(result);
        }

    }

    /**
     * menampilkan pemberitahuan
     * */
    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle("Proses upload")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        filePath = fileUri.getPath();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);


    }

    /**
     * Menerima metode hasil aktivitas akan dipanggil setelah menutup kamera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Jika hasilnya menangkap gambar
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                if (filePath != null) {
;                    // Menampilkan gambar di layar
                    previewMedia(true);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "File tidak ditemukan", Toast.LENGTH_LONG).show();
                }


            } else if (resultCode == RESULT_CANCELED) {

                // Pengguna batalkan pengambilan gambar
                Toast.makeText(getApplicationContext(),
                        "Batal mengambil gambar", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // Gagal menangkap gambar
                Toast.makeText(getApplicationContext(),
                        "Gagal mengambil gambar", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Mengembalikan gambar
     */
    private static File getOutputMediaFile(int type) {

        // Lokasi sdcard eksternal
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Download");

        // Buat direktori penyimpanan jika tidak ada
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Gagal mengupload "
                        + "Download" + " directory");
                return null;
            }
        }

        // Buat nama file media
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    /**
     * Menampilkan foto yang diambil pada layar
     * */
    private void previewMedia(boolean isImage) {
        if (isImage) {
            imageView.setVisibility(View.VISIBLE);
            BitmapFactory.Options options = new BitmapFactory.Options();

            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            Bitmap.createScaledBitmap(bitmap,250,250, true);

            imageView.setImageBitmap(bitmap);

            imageView.setVisibility(View.VISIBLE);
        }
    }
}
