package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FormPembelian extends AppCompatActivity {

    public static Activity fa;


    EditText jumlahikan;

    TextView judulform;

    RadioGroup rgjenispengiriman;

    RadioButton rbkp,rbjp;

    Button pesan,batal;

    Spinner spinkabupaten;
    Spinner spinkecamatan;
    Spinner spinpengiriman;

    private static final String TAG_RESULTPENGIRIMAN = "resultpengiriman";
    private static final String TAG_PENGIRIMAN = "pengiriman";

    private static final String TAG_RESULTKECAMATAN = "resultkecamatan";
    private static final String TAG_KECAMATAN = "kecamatan";

    private static final String TAG_RESULTKABUPATEN = "resultkabupaten";
    private static final String TAG_KABUPATEN = "kabupaten";

    private static final String TAG_RESULTONGKIR = "resultongkir";
    private static final String TAG_ONGKIR = "ongkir";

    static boolean akabupaten=false;
    static boolean akecamatan=false;
    static boolean apengiriman=false;
    static boolean aongkir=false;

    JSONArray rskabupaten = null;
    JSONArray rskecamatan = null;
    JSONArray rspengiriman = null;
    JSONArray rsongkir = null;

    String URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pembelian);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("statuspembelian","").equals("keranjang")){
            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
            editor.putString("idikan", Keranjang.mylistidikan.get(Keranjang.statkeranjang));
            editor.putString("namaikan", Keranjang.mylistidnamaikan.get(Keranjang.statkeranjang));
            editor.putString("hargaikan", Keranjang.mylistidhargaikan.get(Keranjang.statkeranjang));
            editor.putString("stokikan", Keranjang.mylistidstokikan.get(Keranjang.statkeranjang));
            editor.commit();
        }

        fa = this;

        judulform = (TextView) findViewById(R.id.judulform);
        jumlahikan = (EditText) findViewById(R.id.jumlahpembelian);

        pesan = (Button) findViewById(R.id.formpembelianpesan);
        batal = (Button) findViewById(R.id.formpembelianbatal);

        rgjenispengiriman = (RadioGroup) findViewById(R.id.rgjenispengiriman);
        rbkp = (RadioButton) findViewById(R.id.rbkp);
        rbjp = (RadioButton) findViewById(R.id.rbjp);

        spinkabupaten = (Spinner) findViewById(R.id.spinkabupaten);
        spinkecamatan = (Spinner) findViewById(R.id.spinkecamatan);
        spinpengiriman = (Spinner) findViewById(R.id.spinpengiriman);

        judulform.setText("Formulir Pembelian\n"+getSharedPreferences("DATA", MODE_PRIVATE).getString("namaikan",""));

        spinkabupaten.setVisibility(View.GONE);
        spinkecamatan.setVisibility(View.GONE);
        spinpengiriman.setVisibility(View.GONE);

        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
        editor.putString("jenispengiriman", rbkp.getText().toString());
        editor.putString("biayapengiriman","0");
        editor.commit();

        rbkp.setChecked(true);

        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new JSONParseKabupaten().execute();

        spinkabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinkecamatan.setVisibility(View.VISIBLE);
                new JSONParseKecamatan().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinkecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinpengiriman.setVisibility(View.VISIBLE);
                new JSONParsePengiriman().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinpengiriman.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new JSONParseOngkir().execute();
                SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                editor.putString("jenispengiriman", spinpengiriman.getSelectedItem().toString());
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rgjenispengiriman.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbkp){
                    spinkabupaten.setVisibility(View.GONE);
                    spinkecamatan.setVisibility(View.GONE);
                    spinpengiriman.setVisibility(View.GONE);
                    spinkabupaten.setSelection(0);
                    spinkecamatan.setSelection(0);
                    spinpengiriman.setSelection(0);
                    SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                    editor.putString("jenispengiriman", rbkp.getText().toString());
                    editor.putString("biayapengiriman","0");
                    editor.commit();
                }
                else {
                    spinkabupaten.setVisibility(View.VISIBLE);
                }
            }
        });

        pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!jumlahikan.getText().toString().equals("")) {
                    SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                        if (Float.parseFloat(jumlahikan.getText().toString())>Float.parseFloat((getSharedPreferences("DATA", MODE_PRIVATE).getString("stokikan","")))) {
                            Toast.makeText(FormPembelian.this, "Jumlah pemesanan melebihi stok ikan!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            editor.putString("jumlahikan", jumlahikan.getText().toString());
                            editor.commit();
                            startActivity(new Intent(FormPembelian.this,KonfirmasiPembelian.class));
                        }
                    }
                    else{
                        Toast.makeText(FormPembelian.this, "Harap mengisi jumlah pembelian!", Toast.LENGTH_SHORT).show();
                    }
                }

        });

    }

    private class JSONParsePengiriman extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();

            URL = "http://";
            URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            URL += Config.PENGIRIMAN_URL;
            URL += "?kecamatan="+spinkecamatan.getSelectedItem().toString().replace(" ","%20");

            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                apengiriman=false;
            }
            else apengiriman=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(apengiriman==true)
            {
                try{
                    Log.e("status",apengiriman+"");
                    rspengiriman = json.getJSONArray(TAG_RESULTPENGIRIMAN);
                    List<String> lables = new ArrayList<>();
                    lables.add("Pilih Pengiriman");
                    for(int i=0; i<rspengiriman.length();i++)
                    {
                        JSONObject a = rspengiriman.getJSONObject(i);
                        String pengirimanareamuncar = a.getString(TAG_PENGIRIMAN);

                        lables.add(pengirimanareamuncar);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.spinner_text, lables);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinpengiriman.setAdapter(adapter);
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status",apengiriman+"");}
        }

    }

    private class JSONParseKecamatan extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            URL = "http://";
            URL += getSharedPreferences("DATA", MODE_PRIVATE).getString("IP", "");
            URL += Config.KECAMATAN_URL;
            URL += "?kabupaten="+spinkabupaten.getSelectedItem().toString().replace(" ","%20");

            JSONObject json = jParser.getJSONFromUrl(URL);
            if (json == null) {
                akecamatan = false;
            } else akecamatan = true;
            return json;
        }

        protected void onPostExecute(JSONObject json) {
            if (akecamatan == true) {
                try {
                    Log.e("status", akecamatan + "");
                    rskecamatan = json.getJSONArray(TAG_RESULTKECAMATAN);
                    List<String> lables = new ArrayList<>();
                    lables.add("Pilih Kecamatan");
                    for (int i = 0; i < rskecamatan.length(); i++) {
                        JSONObject a = rskecamatan.getJSONObject(i);
                        String kecamatanluarmuncar = a.getString(TAG_KECAMATAN);

                        lables.add(kecamatanluarmuncar);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.spinner_text, lables);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkecamatan.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status", akecamatan + "");
            }
        }

    }

    private class JSONParseKabupaten extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            URL = "http://";
            URL += getSharedPreferences("DATA", MODE_PRIVATE).getString("IP", "");
            URL += Config.KABUPATEN_URL;

            JSONObject json = jParser.getJSONFromUrl(URL);
            if (json == null) {
                akabupaten = false;
            } else akabupaten = true;
            return json;
        }

        protected void onPostExecute(JSONObject json) {
            if (akabupaten == true) {
                try {
                    Log.e("status", akabupaten + "");
                    rskabupaten = json.getJSONArray(TAG_RESULTKABUPATEN);
                    List<String> lables = new ArrayList<>();
                    lables.add("Pilih kabupaten");
                    for (int i = 0; i < rskabupaten.length(); i++) {
                        JSONObject a = rskabupaten.getJSONObject(i);
                        String kabupatenluarmuncar = a.getString(TAG_KABUPATEN);

                        lables.add(kabupatenluarmuncar);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.spinner_text, lables);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinkabupaten.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status", akabupaten + "");
            }
        }

    }

    private class JSONParseOngkir extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute(){

        }
        @Override
        protected JSONObject doInBackground(String... args)
        {
            JSONParser jParser = new JSONParser();

            URL = "http://";
            URL += getSharedPreferences("DATA",MODE_PRIVATE).getString("IP","");
            URL += Config.ONGKIR_URL;
            URL += "?kecamatan="+spinkecamatan.getSelectedItem().toString().replace(" ","%20");
            URL += "&kendaraan="+spinpengiriman.getSelectedItem().toString().replace(" ","%20");

            Log.e("Hasil = ",URL);

            JSONObject json = jParser.getJSONFromUrl(URL);
            if(json==null)
            {
                aongkir=false;
            }
            else aongkir=true;
            return json;
        }
        protected void onPostExecute(JSONObject json) {
            if(aongkir==true)
            {
                try{
                    Log.e("status",aongkir+"");
                    rsongkir = json.getJSONArray(TAG_RESULTONGKIR);
                    for(int i=0; i<rsongkir.length();i++)
                    {
                        JSONObject a = rsongkir.getJSONObject(i);
                        String ongkir = a.getString(TAG_ONGKIR);

                        SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                        editor.putString("biayapengiriman",ongkir);
                        editor.commit();
                    }
                }catch(JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status",aongkir+"");}
        }

    }
}
