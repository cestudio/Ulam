package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Keranjang extends AppCompatActivity {
    public static Activity fa;

    public static ArrayList<String> mylistidikan = new ArrayList<String>();
    public static ArrayList<String> mylistidnamaikan = new ArrayList<String>();
    public static ArrayList<String> mylistidhargaikan = new ArrayList<String>();
    public static ArrayList<String> mylistidstokikan = new ArrayList<String>();
    public static int statkeranjang=0;

    //Web api url
    public static String FEED_URL;

    private static final String TAG = Halaman_Utama_Pembeli.class.getSimpleName();
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapterKeranjang mGridAdapter;
    private ArrayList<GridItem> mGridData;

    public static Button checkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keranjang);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;

        checkout = (Button) findViewById(R.id.btncheckout);

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //grid view
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapterKeranjang(Keranjang.this, R.layout.grid_item_layout_keranjang, mGridData);
        mGridView.setAdapter(mGridAdapter);
        mGridView.setChoiceMode(2);

        FEED_URL = "http://";
        FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.KERANJANG_URL;
        FEED_URL += "?keranjang="+Halaman_Utama_Pembeli.mylist.toString().replace(" ","%20");

        //Start download
        new AsyncHttpTask().execute(FEED_URL);
        mProgressBar.setVisibility(View.VISIBLE);

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Keranjang.mylistidikan.size()<1){
                    Toast.makeText(Keranjang.this, "Anda tidak mempunyai keranjang ikan.", Toast.LENGTH_SHORT).show();
                }
                else{
                    SharedPreferences.Editor editor = getSharedPreferences("DATA", Context.MODE_PRIVATE).edit();
                    editor.putString("statuspembelian", "keranjang");
                    editor.commit();
                    startActivity(new Intent(Keranjang.this,FormPembelian.class));
                }

            }
        });
    }

    //Downloading data
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Download complete. Let us update UI
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);
            } else {
                Toast.makeText(Keranjang.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItem item;
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idikan = post.optString("idikan");
                String namaikan = post.optString("namaikan");
                String hargaikan = post.optString("hargaikan");
                String stokikan = post.optString("stokikan");
                item = new GridItem();
                item.setId(idikan);
                item.setNamaikan(namaikan);
                item.setHarga(hargaikan);
                item.setStok(stokikan);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString("gambarikan"));
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
