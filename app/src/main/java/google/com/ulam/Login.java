package google.com.ulam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    Button login, daftar;

    private static final String TAG_RESULT = "result";
    private static final String TAG_IDUSER = "iduser";
    private static final String TAG_NAMA = "namauser";
    private static final String TAG_ALAMAT = "alamat";
    private static final String TAG_LEVEL = "level";

    static boolean a = false;

    JSONArray rs = null;

    EditText username, password;

    TextView ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ip = (TextView) findViewById(R.id.ip);
        login = (Button) findViewById(R.id.loginmasuk);
        daftar = (Button) findViewById(R.id.logindaftar);

        username = (EditText) findViewById(R.id.loginusername);
        password = (EditText) findViewById(R.id.loginpassword);

        ip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IP ip = new IP(Login.this);
                ip.show();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendLoginUser().execute();
            }
        });

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Login.this, Register.class));
                finish();
            }
        });
    }

    //JSON parser, mengambil data dari web service,
    private class SendLoginUser extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute() {

        }

        //menjalankan proses di background, tidak mengganggu proses lain
        @Override
        protected JSONObject doInBackground(String... args) {
            String URL;
            URL = "http://";
            URL += getSharedPreferences("DATA", MODE_PRIVATE).getString("IP", "");
            URL += Config.LOGINUSER_URL;
            URL += "?username=" + username.getText().toString();
            URL += "&password=" + password.getText().toString();
            URL += "&token=" + getSharedPreferences("DATA", MODE_PRIVATE).getString("token", "");

            Log.e("Hasil =", URL);

            //Membuat JSON Parser instance
            JSONParser jParser = new JSONParser();

            //mengambil JSON String dari url
            JSONObject json = jParser.getJSONFromUrl(URL);
            if (json == null) {
                a = false;
            } else a = true;
            return json;
        }

        protected void onPostExecute(JSONObject json) {
            if (a == true) {
                try {
                    Log.e("status", a + "");
                    rs = json.getJSONArray(TAG_RESULT);
                    for (int i = 0; i < rs.length(); i++) {
                        JSONObject a = rs.getJSONObject(i);
                        String iduser = a.getString(TAG_IDUSER);

                        if (iduser.equals("0")) {
                            Toast.makeText(Login.this, "Username/Password Anda Salah!", Toast.LENGTH_SHORT).show();
                        } else {
                            String nama = a.getString(TAG_NAMA);
                            String alamat = a.getString(TAG_ALAMAT);
                            String level = a.getString(TAG_LEVEL);
                            SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                            editor.putString("statlogin", "1");
                            editor.putString("iduser", iduser);
                            editor.putString("nama", nama);
                            editor.putString("alamat", alamat);
                            editor.commit();

                            if (level.equals("1")) {
                                startActivity(new Intent(Login.this, Halaman_Utama_User.class));
                                finish();
                                Halaman_Utama.fa.finish();
                                editor.putString("status", "pengelola").commit();
                            } else {
                                startActivity(new Intent(Login.this, Halaman_Utama_Pembeli.class));
                                finish();
                                Halaman_Utama.fa.finish();
                                editor.putString("status", "pembeli").commit();
                            }


                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //Jika tidak ada koneksi atau server down, keluarkan message error
            else {

                Toast.makeText(getApplicationContext(), "Tidak terkoneksi dengan server", Toast.LENGTH_SHORT).show();
                Log.e("status", a + "");
            }
        }

    }
}


