package google.com.ulam;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Halaman_Utama_Pembeli extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static ArrayList<String> mylist = new ArrayList<String>();

    public static Activity fa;

    //Web api url
    public static String FEED_URL;

    private static final String TAG = Halaman_Utama_Pembeli.class.getSimpleName();
    private GridView mGridView;
    private ProgressBar mProgressBar;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;

    public static TextView keranjang;

    ImageView ickeranjang;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        public void run() {
            new AsyncHttpTask().execute(FEED_URL);
            mProgressBar.setVisibility(View.VISIBLE);
            handler.postDelayed(runnable, 1000);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman__utama_pembeli);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fa = this;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);//pembuatan drawer
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        keranjang = (TextView) findViewById(R.id.tvkeranjang);
        ickeranjang=(ImageView) findViewById(R.id.ivkeranjang);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View header= navigationView.getHeaderView(0);//menampilkan alamat dan nama
        TextView username=(TextView) header.findViewById(R.id.nav_namauser);
        TextView email=(TextView) header.findViewById(R.id.nav_alamatuser);
        username.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("nama", ""));
        email.setText(getSharedPreferences("DATA",MODE_PRIVATE).getString("alamat", ""));

        mGridView = (GridView) findViewById(R.id.gridView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //grid view
        mGridData = new ArrayList<>();
        mGridAdapter = new GridViewAdapter(Halaman_Utama_Pembeli.this, R.layout.grid_item_layout, mGridData);
        mGridView.setAdapter(mGridAdapter);

        FEED_URL = "http://";
        FEED_URL += getSharedPreferences("DATA", Context.MODE_PRIVATE).getString("IP","");
        FEED_URL += Config.HALAMANUTAMA_URL;

        //download
        new AsyncHttpTask().execute(FEED_URL);

        mProgressBar.setVisibility(View.VISIBLE);

        ickeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Halaman_Utama_Pembeli.this,Keranjang.class));
            }
        });
    }
//klik tombol back drawer akan tertutup
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // klik navigation
        int id = item.getItemId();

        if (id == R.id.nav_Beranda) {

        } else if (id == R.id.nav_Pembayaran) {
            startActivity(new Intent(Halaman_Utama_Pembeli.this,Pembayaran.class));
        } else if (id == R.id.nav_Riwayat_Pemesanan) {
            startActivity(new Intent(Halaman_Utama_Pembeli.this,RiwayatPemesanan.class));
        } else if (id == R.id.nav_bantuan_rekening) {
            BantuanRekening bantuanrekening = new BantuanRekening(Halaman_Utama_Pembeli.this);
            bantuanrekening.setCancelable(false);
            bantuanrekening.show();
        } else if (id == R.id.nav_bantuan_alamat) {
            BantuanAlamat bantuanalamat = new BantuanAlamat(Halaman_Utama_Pembeli.this);
            bantuanalamat.setCancelable(false);
            bantuanalamat.show();
        } else if (id == R.id.nav_Keluar) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Halaman_Utama_Pembeli.this);
            alertDialogBuilder.setMessage("Apa anda yakin ingin keluar dari aplikasi ini?");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setNegativeButton("Ya",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    SharedPreferences.Editor editor = getSharedPreferences("DATA", MODE_PRIVATE).edit();
                    editor.putString("statlogin", "0");
                    editor.commit();
                    startActivity(new Intent(Halaman_Utama_Pembeli.this,Halaman_Utama.class));
                    finish();
                }
            });

            alertDialogBuilder.setPositiveButton("Tidak",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Downloading data asynchronously
    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            Integer result = 0;
            try {
                // Create Apache HttpClient
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse httpResponse = httpclient.execute(new HttpGet(params[0]));
                int statusCode = httpResponse.getStatusLine().getStatusCode();

                // 200 represents HTTP OK
                if (statusCode == 200) {
                    String response = streamToString(httpResponse.getEntity().getContent());
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    result = 0; //"Failed
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // download berhasil
            if (result == 1) {
                mGridAdapter.setGridData(mGridData);
            } else {
                Toast.makeText(Halaman_Utama_Pembeli.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
            mProgressBar.setVisibility(View.GONE);
        }
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }

        // Close stream
        if (null != stream) {
            stream.close();
        }
        return result;
    }

    /**
     * Parsing the feed results and get the list
     * @param result
     */
    private void parseResult(String result) {
        try {
            JSONObject response = new JSONObject(result);
            JSONArray posts = response.optJSONArray("result");
            GridItem item;
            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.optJSONObject(i);
                String idikan = post.optString("idikan");
                String namaikan = post.optString("namaikan");
                String hargaikan = post.optString("hargaikan");
                String stokikan = post.optString("stokikan");
                item = new GridItem();
                item.setId(idikan);
                item.setNamaikan(namaikan);
                item.setHarga(hargaikan);
                item.setStok(stokikan);
                if (null != posts && posts.length() > 0) {
                    JSONObject attachment = posts.getJSONObject(i);
                    if (attachment != null)
                        item.setGambar(attachment.getString("gambarikan"));
                }

                mGridData.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
